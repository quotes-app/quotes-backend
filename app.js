const express = require("express");
const app = express();
const cors = require("cors");
const axios = require("axios");
const config = require("./config");
var corsOptions = {
  origin: config.FrontEndUri,
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
app.use(cors(corsOptions)); //allows external sites to access server

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.send("Welcome to datQuote's API server " + config.FrontEndUri);
});

app.get("/api/programming", (req, res) => {
  quote().then(data => {
    res.status(200);
    res.json({ quotes: data });
  });
});

app.post("/api/world", (req, res) => {
  console.log(req.body);
  res.send(
    `I received your POST request. This is what you sent me: ${req.body.post}`
  );
});

async function quote() {
  return axios
    .get("http://quotes.stormconsultancy.co.uk/popular.json")
    .then(res => {
      return res.data;
    });
}

module.exports = app;
